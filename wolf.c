#include "esa.h"

#include "Animal.h"

esa(animals, struct Animal);
esa(characters, char);

esa_push(animals)    = { "a wolf", "Rickard", 47 };
esa_push(animals)    = { "a wolf", "Ronja",   48 };
esa_push(characters) = { '?' };

