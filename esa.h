#pragma once

#define esa_member __attribute__((aligned ()))

#define esa( NAME, TYPE )\
    typedef struct { esa_member TYPE x; } _esacontainer_##NAME;\
    __attribute__((weak)) __attribute__((used)) struct {} const __esa_start_esa_##NAME;\
    __attribute__((weak)) __attribute__((used)) struct {} const __esa_stop_esa_##NAME;\
    extern _esacontainer_##NAME __start_esa_##NAME;\
    extern _esacontainer_##NAME __stop_esa_##NAME;

#define esa_push( NAME ) _esa_push( NAME, __COUNTER__ )
#define _esa_push( NAME, ID ) __esa_push( NAME, ID )
#define __esa_push( NAME, ID )\
    __attribute__((__used__))\
    __attribute__((section("esa_" #NAME )))\
    static esa_member _esacontainer_##NAME _esa_##NAME##_##ID  

#define esa_for( NAME )\
    for\
    (\
        typeof(__start_esa_##NAME.x) *it = (typeof(__start_esa_##NAME.x) *)(&__start_esa_##NAME);\
        (_esacontainer_##NAME *)it < &__stop_esa_##NAME;\
        it = (typeof(__start_esa_##NAME.x) *)(((_esacontainer_##NAME *)it) + 1)\
    )

