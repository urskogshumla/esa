#include "esa.h"

#include <stdio.h>

struct Animal
{
    char *type, *name, age;
};

esa(animals, struct Animal);
esa(characters, char);

int main()
{
    esa_for(animals) printf("Hi my name is %s and i am %d months old.\n", it->name, (int)(it->age));
    esa_for(animals) printf("%s is %s.\n", it->name, it->type);
    esa_for(characters) printf("I am a byte and my value is %c\n", *it);
    printf("Done.\n");
}
