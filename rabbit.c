#include "esa.h"

#include "Animal.h"

esa(animals, struct Animal);
esa(characters, char);

esa_push(animals)[] =
{
    { "a rabbit", "Gunnar", 23 },
    { "a rabbit", "Ullis",  53 },
    { "a rabbit", "Gösta",  51 },
    { "a rabbit", "Fingal", 5 },
    { "a rabbit", "Pierre", 11 }
};

esa_push(characters) = { '+' };
esa_push(characters) = { '-' };
esa_push(characters)[] = { 'X', 'Y', 'Z' };

